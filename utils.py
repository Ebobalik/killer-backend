import random
import copy

class Player:
    def __init__(self,name,uid:str,tg_id,isAlive=True,target=None,):
        self.name = name
        self.isAlive = isAlive
        self.target = target
        self.excomunnicado = False
        self.kills = 0
        self.killer = None
        self.qr_id = uid
        self.tg_id = tg_id
    def __repr__(self):
        return f"{self.name} {self.isAlive} {None if self.target == None else self.target.name} {self.excomunnicado} {self.kills} {None if self.killer == None else self.killer.name}"
    


class ContractsGenerator:
    def __init__(self,players:list):
        self.players = players
        self.contracts = {}
    
    def generate(self):
        players = copy.copy(self.players)
        random.shuffle(players)
        for i in range(0,len(players)):
            self.contracts[players[i]] = players[(i+1)%len(players)]
            players[i].target = players[(i+1)%len(players)]
            players[(i+1)%len(players)].killer = players[i]


class GameManager:
    def __init__(self,players:list):
        self.gameStarted = False
        self.players = players
        self.contract_generator = ContractsGenerator(players=players)
        self.first_kill = {}
        self.first_kill['status'] = False
        self.first_kill['killer'] = None
        self.first_kill['victim'] = None

    def StartGame(self):
        self.contract_generator.generate()
        self.gameStarted = True
        #print(self.players)

    def GetAllPlayers(self):
        result = {}
        for player in self.players:
            d = {}
            d['target'] = player.target
            d['kills'] = player.kills
            d['killer'] = None if not self.gameStarted else player.killer.name
            d['excomunnicado'] = player.excomunnicado
            d['isAlive'] = player.isAlive
            result[player.name] = d
        return result

    def SetFirstKill(self,killer:Player,victim:Player):
        self.first_kill['killer'] = killer.name
        self.first_kill['victim'] = victim.name
        self.first_kill['status'] = True

    def GetAlivePlayers(self):
        result = {}
        for player in self.players:
            if not player.isAlive:
                continue
            d = {}
            d['target'] = None if not self.gameStarted else player.target.name
            d['kills'] = player.kills
            d['killer'] = None if not self.gameStarted else player.killer.name
            d['excomunnicado'] = player.excomunnicado
            d['isAlive'] = player.isAlive
            result[player.name] = d
        return result
    

    def AddPlayer(self,name,id,tg_id):
        self.players.append(Player(name,uid=id,tg_id=tg_id,))

    def KillPlayer(self, killer:Player, victim:Player):
        if killer.target.name == victim.name:
            self.KillDefualtVictim(killer,victim)
        if victim.excomunnicado == True:
            self.KillExcomunnicado(killer,victim)
    
    def KillDefualtVictim(self,killer:Player, victim:Player) -> bool:
        if not self.gameStarted:
            print("[INFO] Game not started")
            return False
        if type(killer)==bool or type(victim)==bool:
            print(killer)
            print(victim)
            print("Incorrect type")
            return False
        if not killer.isAlive or not victim.isAlive:
            print("[INFO] Someone already death")
            return False
        #print(killer.target.name,victim.name)
        if killer.target.name == victim.name:
            #print(killer.target,victim.target)
            victim.isAlive = False
            killer.target = victim.target
            killer.kills +=1
            victim.target = None
            print(killer.name,'killed',victim.name)
            if self.first_kill['status']==False:
                self.SetFirstKill(killer,victim)
            return True
        return False
     
    def KillExcomunnicado(self,killer:Player, victim:Player) -> bool:
        if not self.gameStarted:
            return False
        
        if not victim.excomunnicado:
            return False
        if victim.isAlive == False:
            return False
        victim.isAlive = False
        killer.kills +=1
        victim.killer.target = victim.target
        killer.excomunnicado = True
        return True

    def SetExcomunnicadoStaus(self,tg_id,isEx:bool):
        for i in range(len(self.players)):
            if self.players[i].tg_id == tg_id:
                self.players[i].excomunnicado = isEx

    def GetPlayerByName(self,name):
        for player in self.players:
            if player.name == name:
                return player
        return None
    
    def GetPlayerByQRId(self,id):
        for player in self.players:
            print(player.tg_id,id)
            if player.qr_id == id:
                return player
        return False
    
    def GetPlayerByTgId(self,tgid):
        for player in self.players:
            #print(player.tg_id,tgid)
            if player.tg_id == tgid:
                return player
        return False
#8ef45a3a-411d-47c7-a272-700591dbf5f6 - kirill
