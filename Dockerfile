FROM python:latest
#RUN mkdir /app

COPY . /app
WORKDIR /app
RUN pip install -r /app/req.txt
EXPOSE 5000
ENTRYPOINT ["python","main.py"]