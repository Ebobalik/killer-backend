from flask import Flask, request, make_response,jsonify
from flask_sqlalchemy import SQLAlchemy
from utils import GameManager
import uuid
from transliterate import translit
from string import ascii_letters


class MyFlaskApp(Flask):
  def run(self, host=None, port=None, debug=None, load_dotenv=True, **options):
    if 1:
      with self.app_context():
        on_startup()
    super(MyFlaskApp, self).run(host=host, port=port, debug=debug, load_dotenv=load_dotenv, **options)


def validate(nickname):
    nickname = "".join(nickname.split(' '))
    t=all(map(lambda c: c in ascii_letters, nickname))
    print(t)
    return t

def on_startup():
    players = Player.query.all()
    for p in players:
        gm.AddPlayer(p.username,p.uuid,p.tg_id)
    print("OK")
    return "Ok"

app = MyFlaskApp(__name__)
app.config['SQLALCHEMY_DATABASE_URI']="postgresql://killer-admin:verysecretkey@172.28.0.2:5432/killer-db"
app.config['JSON_AS_ASCII'] = False
db = SQLAlchemy(app)
gm = GameManager([])


class Player(db.Model):
    __tablename__="users"
    username = db.Column(db.Text())
    tg_id = db.Column(db.Text())
    kills = db.Column(db.Integer())
    target = db.Column(db.Text())
    killer = db.Column(db.Text())
    uuid = db.Column(db.Text(),primary_key=True)
    isalive = db.Column(db.Boolean())
    excomunnicado = db.Column(db.Boolean())


@app.route("/players")
def get_players():
    ps = gm.players
    response = []
    for p in ps:
        response.append([p.name ,p.isAlive,p.qr_id,p.tg_id,('none' if p.target==None else p.target.name)])
        #print(p.username)
    return make_response(response)

@app.route("/players/getAllTG/")
def get_all_tg_players():
    players = gm.players
    response = []
    for p in players:
        response.append(p.tg_id)
    return response

@app.route("/players/<tg_id>/")
def get_player_info(tg_id):
    player = gm.GetPlayerByTgId(tgid=tg_id)
    if not player:
        return jsonify(message="not found"),404
    response = {}
    response ['username'] = player.name
    response ['tg_id'] = player.tg_id
    response ['kills'] = player.kills
    response ['killer'] = ("none" if player.killer == None else player.killer.name)
    response ['target'] = ("none" if player.target == None else player.target.name)
    response ['isAlive'] = player.isAlive
    response ['excomunnicado'] = player.excomunnicado
    response ['uuid'] = player.qr_id
    print(response)
    return response

@app.route("/players/add/")
def create_player():
    if gm.gameStarted:
        return "forbidden",403
    username = request.args.get('username')
    tgid = request.args.get('tgid')
    player  = Player.query.filter_by(tg_id = tgid).first()
    if player:
        return jsonify(message="player already exists")
    print(tgid)
    print(username)
    #username = translit(username,'ru',reversed=True)
    user = Player(username=username,tg_id=tgid,uuid = str(uuid.uuid4()),kills=0,target="none",killer="none",isalive=True,excomunnicado=False)
    db.session.add(user)
    db.session.commit()
    gm.AddPlayer(username,user.uuid,tgid)
    return jsonify(message="success")

@app.route("/players/alive/")
def get_alive_players():
    players = gm.GetAlivePlayers()
    print(players)
    return [x for x in players]

@app.route("/players/dead/")
def get_dead_players():
    response = []
    for player in gm.players:
        if player.isAlive==False:
            response.append(f'{player.name} was killed by {player.killer.name}')
    return response
@app.route('/players/get_contract/<tg_id>/')
def get_contract(tg_id):
    p = gm.GetPlayerByTgId(tgid=tg_id)
    if p==False:
        return jsonify(message="failed"),404
    print(p.target)
    return jsonify(target=("none" if p.target==None else p.target.name),alive=p.isAlive)

@app.route("/players/all/")
def get_all_players():
    print(gm.players)
    players = gm.GetAllPlayers()
    return players

@app.route("/players/get_top")
def get_top_players():
    resp = []
    for pl in gm.players:
        resp.append([pl.name,pl.kills,pl.alive])
    return resp


@app.route("/test/")
def test():
    return "ебало сломаю"

@app.route("/game/status")
def game_status():
    return jsonify(started = gm.gameStarted)

@app.route("/game/start")
def start_game():
    gm.StartGame()
    return jsonify(started = gm.gameStarted)

@app.route('/game/stop')
def stop_game():
    gm.gameStarted = False
    return jsonify(started = gm.gameStarted)

@app.route('/game/restart')
def restart_game():
    gm.StartGame()
    for i in range(len(gm.players)):
        gm.players[i].kills = 0
    return "restarted"
@app.route("/set_excomunnicado/<tg_id>/<sett>")
def set_excomunnicado(tg_id,sett):
    if sett=="yes":
        sett = True
    else:
        sett = False
    gm.SetExcomunnicadoStaus(tg_id,sett)
    return jsonify(message="success")

@app.route("/game/kill/<killer_id>/<victim_qrid>")
def try_kill(killer_id,victim_qrid):
    killer = gm.GetPlayerByTgId(killer_id)
    victim = gm.GetPlayerByQRId(victim_qrid)
    db_killer = Player.query.filter_by(tg_id=killer_id).first()
    db_victim = Player.query.filter_by(uuid=victim_qrid).first()

    if not db_victim:
        return "Player not found",404
    
    if gm.KillPlayer(killer,victim):
        db_killer.kills +=1
        db_victim.isAlive = False
        db.session.commit()
        return db_victim.tg_id
    else:
        return "Failed",404

if __name__=="__main__":
    app.run(debug=True,host="0.0.0.0",port=5000)
